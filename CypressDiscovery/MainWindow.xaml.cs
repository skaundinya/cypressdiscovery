﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Net.NetworkInformation;
using System.Windows.Threading;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace CypressDiscovery
{
    //
    // Implement Cypress Discovery Protocol Service (CDPS)
    //
    public partial class MainWindow : Window
    {
        // Delegate for callbacks
        public delegate void processClientPacketDelegate(byte[] receiveBytes, IPEndPoint remoteEP);

        // Commands
        public const int COMMAND_POLL = 0x01;
        public const int COMMAND_ACK = 0x02;
        public const int COMMAND_CONFIGURE = 0x03;
        public const int COMMAND_REBOOT = 0x04;

        // Subcommands
        public const int SUBCOMMAND_PRODUCT_ID = 0x01;
        public const int SUBCOMMAND_MAC_ADDRESS = 0x02;
        public const int SUBCOMMAND_IP_ADDRESS = 0x03;
        public const int SUBCOMMAND_SUBNET_MASK = 0x04;
        public const int SUBCOMMAND_GATEWAY = 0x05;
        public const int SUBCOMMAND_DNS_SERVER = 0x06;
        public const int SUBCOMMAND_DHCP_ENABLED = 0x07;
        public const int SUBCOMMAND_WEB_SERVER_PORT = 0x08;
        public const int SUBCOMMAND_TELNET_PORT = 0x09;
        public const int SUBCOMMAND_SERIAL_NUMBER = 0x0A;
        public const int SUBCOMMAND_FIRMWARE_VERSION = 0x0B;
        public const int SUBCOMMAND_HARDWARE_VERSION = 0x0C;
        public const int SUBCOMMAND_PRODUCT_NAME = 0x0D;
        public const int SUBCOMMAND_PRODUCT_DESCRIPTION = 0x0E;

        // Max field lengths
        public const int MAX_PRODUCT_ID_LEN = 2;
        public const int MAX_VERSION_LEN = 10;
        public const int MAX_PRODUCT_NAME_LEN = 20;
        public const int MAX_PRODUCT_DESCRIPTION_LEN = 30;

        // Poll packet
        public const int PP_COMMAND = 0;
        public const int PP_LENGTH = 1;
        public const int PP_CRC = 2;
        public const int PP_PACKET_LEN = 4;

        // Ack packet - payload is full of TLV structures
        public const int AP_COMMAND = 0;
        public const int AP_LENGTH = 1;
        public const int AP_DATA = 2;

        // Multicast address
        public const string CYPRESS_MCAST_ADDRESS = "225.12.13.14";

        // UDP Ports
        public const int CYPRESS_SEND_PORT = 31001;
        public const int CYPRESS_LISTEN_PORT = 31002;

        // Member variables
        DispatcherTimer _timer;
        bool _bBroadcastToggle;

        // Debugging
        int debug_count;

        // UDP broadcast Socket
        UdpClient _client_socket;

        // UDP multicast socket
        Socket _mcast_socket;
        IPAddress _mcast_address;
        int _mcast_port;

        // UDP listen socket
        UdpClient _listen_socket;

        // Threads
        Thread _listen_thread;

        // Constructor
        public MainWindow()
        {
            InitializeComponent();

            // Initialize member variables
            _timer = null;
            _bBroadcastToggle = false;
            _listen_thread = new Thread(new ThreadStart(ListenProc));
            _listen_thread.IsBackground = true;
            _mcast_address = IPAddress.Parse(CYPRESS_MCAST_ADDRESS);
            _mcast_port = CYPRESS_SEND_PORT;
        }

        // Start discovery logic
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Send start message
            report("Starting...");
            // Create broadcast socket
            _client_socket = new UdpClient(0);
            _client_socket.EnableBroadcast = true;
            // Create multicast socket
            joinMulticastGroup();
            // Start a listen thread
            _listen_thread.Start();
            // Start a polling timer
            _timer = new DispatcherTimer();
            _timer.Interval = new TimeSpan(0, 0, 3);
            _timer.Tick += timer_Poll;
            _timer.Start();
        }

        // Create and manage a UPD socket connection
        public void ListenProc()
        {
            try
            {
                _listen_socket = new UdpClient(CYPRESS_LISTEN_PORT);
            }
            catch (Exception ex)
            {
                report("Problem creating listen socket: " + ex.Message);
                return;
            }
            while (1 > 0)
            {
                readClientPacket();
            }
        }

        // Read an process an incoming client packet
        private void readClientPacket()
        {
            // Blocking call to read client packets
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Any, 0);
            try
            {
                Byte[] receiveBytes = _listen_socket.Receive(ref remoteEP);
                processClientPacket(receiveBytes, remoteEP);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ReadClientPacket: Got Exception " + ex.Message);
            }
        }

        // Print the contents of a client packet
        private void processClientPacket(byte[] receiveBytes, IPEndPoint remoteEP)
        {
            // Dispatch on the GUI thread
            if (!this.Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new processClientPacketDelegate(processClientPacket), new Object[] { receiveBytes, remoteEP });
                return;
            }
            report("New packet received from: " + remoteEP.Address.ToString());
            for (int i = 0; i < receiveBytes.Length; i++)
            {
                reportByte(receiveBytes[i]);
            }
            report("");
            report("");
        }

        // Poll for new devices on the network
        void timer_Poll(object sender, EventArgs e)
        {
            _bBroadcastToggle = !_bBroadcastToggle;
            if (_bBroadcastToggle)
            {
                report("Sending broadcast poll");
                sendBroadcastPollPacket();
            }
            else
            {
                report("Sending multicast poll");
                sendMulticastPollPacket();
            }

            // Debugging
            debug_count++;
            if (debug_count == 2)
            {
                byte[] test_packet = new byte[] {
                    0x02, 0x73, 0x01, 0x02, 0x36, 0x22, 0x02, 0x06, 0xF8, 0x22, 0x85, 0x00, 0x04, 0xB2, 0x03,
                    0x04, 0xAC, 0x10, 0x03, 0x90, 0x04, 0x04, 0xFF, 0xFF, 0xFF, 0x00, 0x05, 0x04, 0xAC, 0x10,
                    0x03, 0x01, 0x06, 0x04, 0x00, 0x00, 0x00, 0x00, 0x07, 0x01, 0x01, 0x08, 0x02, 0x50, 0x00,
                    0x09, 0x02, 0x17, 0x00, 0x0A, 0x08, 0x53, 0x4E, 0x3A, 0x32, 0x32, 0x33, 0x36, 0x00, 0x0B,
                    0x0A, 0x76, 0x32, 0x2E, 0x30, 0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x06, 0x76, 0x31,
                    0x2E, 0x30, 0x30, 0x00, 0x0D, 0x0A, 0x43, 0x44, 0x50, 0x57, 0x2D, 0x4B, 0x31, 0x55, 0x53,
                    0x00, 0x0E, 0x18, 0x49, 0x50, 0x20, 0x74, 0x6F, 0x20, 0x42, 0x75, 0x74, 0x74, 0x6F, 0x6E,
                    0x20, 0x57, 0x61, 0x6C, 0x6C, 0x2D, 0x50, 0x6C, 0x61, 0x74, 0x65, 0x00, 0xD6, 0x99
                };
                parse_packet(test_packet);
            }
            
        }

        // Parse an ACK packet with a header followed by multiple TLV fields
        private bool parse_packet(byte[] packet)
        {
            // Check minimum packet length
            if (packet.Length < 4)
            {
                report("ERR: Short packet length");
                return false;
            }
            // Check packet type
            if (packet[AP_COMMAND] != COMMAND_ACK)
            {
                report("ERR: Unexpected command in received packet");
                return false;
            }
            // Extract length
            int len = packet[AP_LENGTH];
            if (len != packet.Length - 4) {
                report("ERR: Unexpected length in received packet");
                return false;
            }
            // Verify checksum
            ushort checksum_1 = NetworkUtility.calc_crc(0xFFFF, packet, AP_DATA, len);
            ushort checksum_2 = (ushort) (packet[AP_DATA + len] + packet[AP_DATA + len + 1] * 256);
            if (checksum_1 != checksum_2)
            {
                report("ERR: Checksum failed in received packet");
                return false;
            }
            // Process TLV fields
            int index = 0;
            while (index < len)
            {
                int subcommand = packet[AP_DATA + index];
                int subcommand_len = packet[AP_DATA + index + 1];
                int subcommand_data = AP_DATA + index + 2;
                if (index + subcommand_len + 2 > len)
                {
                    report("ERR: Unexpected TLV length in received packet");
                    return false;
                }
                if (!process_tlv(packet, subcommand, subcommand_len, subcommand_data))
                {
                    report("ERR: Failed to parse TLV field [" + subcommand + "] in received packet");
                    return false;
                }
                index += subcommand_len + 2;
            }
            if (index != len)
            {
                report("ERR: Unexpected structure in received packet");
                return false;
            }
            report("Packet parsed successfully");
            return true;
        }

        // Parse a TLV field in an ACK packet
        private bool process_tlv(byte[] packet, int subcommand, int subcommand_len, int subcommand_data)
        {
            switch (subcommand)
            {
                case SUBCOMMAND_PRODUCT_ID:
                    return parse_product_id(packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_MAC_ADDRESS:
                    return parse_mac_address(packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_IP_ADDRESS:
                    return parse_ip("IP ADDRESS", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_SUBNET_MASK:
                    return parse_ip("SUBNET MASK", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_GATEWAY:
                    return parse_ip("GATEWAY", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_DNS_SERVER:
                    return parse_ip("DNS SERVER", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_DHCP_ENABLED:
                    return parse_bool("DHCP ENABLED", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_WEB_SERVER_PORT:
                    return parse_port("WEB SERVER PORT", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_TELNET_PORT:
                    return parse_port("TELNET PORT", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_SERIAL_NUMBER:
                    return parse_string("SERIAL NUMBER", 10, packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_FIRMWARE_VERSION:
                    return parse_string("FIRMWARE VERSION", 10, packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_HARDWARE_VERSION:
                    return parse_string("HARDWARE VERSION", 10, packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_PRODUCT_NAME:
                    return parse_string("PRODUCT NAME", 20, packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_PRODUCT_DESCRIPTION:
                    return parse_string("PRODUCT DESCRIPTION", 30, packet, subcommand_data, subcommand_len);
            }
            return false;
        }

        // Parse a 16 bit product ID
        private bool parse_product_id(byte[] packet, int index, int len)
        {
            if (len != 2)
            {
                return false;
            }
            string parse_result = String.Format("> Product ID = {0:X2}:{1:X2}", packet[index + 1], packet[index]);
            report(parse_result);
            return true;
        }

        // Parse a 6 byte MAC address
        private bool parse_mac_address(byte[] packet, int index, int len)
        {
            if (len != 6)
            {
                return false;
            }
            string parse_result = String.Format("> MAC Address = {0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}",
                packet[index],
                packet[index + 1],
                packet[index + 2],
                packet[index + 3],
                packet[index + 4],
                packet[index + 5]);
            report(parse_result);
            return true;
        }

        // Parse an IP address field
        private bool parse_ip(string field_name, byte[] packet, int index, int len)
        {
            if (len != 4)
            {
                return false;
            }
            try
            {
                string parse_result = String.Format("> {0} = {1}", field_name, NetworkUtility.IpAddressToString(packet, index));
                report(parse_result);
            }
            catch
            {
                // Problem parsing IP address
                return false;
            }
            return true;
        }

        // Parse a boolean field
        private bool parse_bool(string field_name, byte[] packet, int index, int len)
        {
            if (len != 1)
            {
                return false;
            }
            string parse_result = String.Format("> {0} = {1}", field_name, Convert.ToBoolean(packet[index]));
            report(parse_result);
            return true;
        }

        // Parse a port field
        private bool parse_port(string field_name, byte[] packet, int index, int len)
        {
            if (len != 2)
            {
                return false;
            }
            string parse_result = String.Format("> {0} = {1}", field_name, packet[index] + packet[index + 1] * 256);
            report(parse_result);
            return true;
        }

        // Parse an ASCII string
        private bool parse_string(string field_name, int max_length, byte[] packet, int index, int len)
        {
            if (len > max_length)
            {
                return false;
            }
            try
            {
                string parse_result = String.Format("> {0} = {1}", field_name, NetworkUtility.BytesToString(packet, index, len));
                report(parse_result);
            }
            catch
            {
                // Problem parsing ASCII string
                return false;
            }
            return true;
        }

        // Send broadcast poll packet
        private void sendBroadcastPollPacket()
        {
            Byte[] sendBytes = new Byte[PP_PACKET_LEN];
            sendBytes[PP_COMMAND] = COMMAND_POLL;
            sendBytes[PP_LENGTH] = 0x00;
            sendBytes[PP_CRC] = 0xFF;
            sendBytes[PP_CRC + 1] = 0xFF;
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Broadcast, CYPRESS_SEND_PORT);
            try
            {
                _client_socket.Send(sendBytes, sendBytes.Length, remoteEP);
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendBroadcastPollPacket: Got Exception " + ex.Message);
            }
        }

        // Send multicast poll packet
        private void sendMulticastPollPacket()
        {
            Byte[] sendBytes = new Byte[PP_PACKET_LEN];
            sendBytes[PP_COMMAND] = COMMAND_POLL;
            sendBytes[PP_LENGTH] = 0x00;
            sendBytes[PP_CRC] = 0xFF;
            sendBytes[PP_CRC + 1] = 0xFF;
            IPEndPoint remoteEP = new IPEndPoint(_mcast_address, _mcast_port);
            try
            {
                _mcast_socket.SendTo(sendBytes, remoteEP);
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendMulticastPollPacket: Got Exception " + ex.Message);
            }
        }

        // Join a multicast group
        private void joinMulticastGroup()
        {
            try
            {
                // Create a multicast socket.
                _mcast_socket = new Socket(AddressFamily.InterNetwork,
                                         SocketType.Dgram,
                                         ProtocolType.Udp);

                // Get the local IP address used by the listener and the sender to
                // exchange multicast messages. 
                IPAddress localIPAddr = IPAddress.Parse(my_ip_address());

                // Create an IPEndPoint object. 
                IPEndPoint IPlocal = new IPEndPoint(localIPAddr, 0);

                // Bind this endpoint to the multicast socket.
                _mcast_socket.Bind(IPlocal);

                // Define a MulticastOption object specifying the multicast group 
                // address and the local IP address.
                // The multicast group address is the same as the address used by the listener.
                MulticastOption mcastOption;
                mcastOption = new MulticastOption(_mcast_address, localIPAddr);

                _mcast_socket.SetSocketOption(SocketOptionLevel.IP,
                                            SocketOptionName.AddMembership,
                                            mcastOption);

            }
            catch (Exception ex)
            {
                Console.WriteLine("JoinMulticastGroup: Got Exception " + ex.Message);
            }
        }

        // My IP address
        string my_ip_address()
        {
            // Suggested approach from Stack Overflow web site
            return NetworkInterface.GetAllNetworkInterfaces()
                .SelectMany(adapter => adapter.GetIPProperties().UnicastAddresses)
                .Where(adr => adr.Address.AddressFamily == AddressFamily.InterNetwork && adr.IsDnsEligible)
                .Select(adr => adr.Address.ToString()).FirstOrDefault();
        }

        // Append to console output
        void report(string msg)
        {
            tbConsole.Text += msg;
            tbConsole.Text += "\r\n";
        }

        // Append to console output
        void reportByte(byte b)
        {
            tbConsole.Text += String.Format("{0:X2} ", b);
        }
    }
}
